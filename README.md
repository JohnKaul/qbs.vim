# README #
This is a Vim syntax file for the QBS (Qt Build System). 

### How do I get set up? ###
If you don't have a preferred installation method, I recommend installing [pathogen.vim](https://github.com/tpope/vim-pathogen), and then simply copy and paste:

    cd ~/.vim/bundle
    git clone https://JohnKaul@bitbucket.org/JohnKaul/qbs.vim.git

Otherwise you can always download this and place the `qbs.vim` file in the `syntax` directory. 

### Contribution guidelines ###

* Contribute? Please. Feel free.
* Code review? Yes, please.
* Comments? Yes, please.

### Who do I talk to? ###

* John Kaul - john.kaul@outlook.com